﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace DemoAirlines.Services
{
    public class AuthOptions
    {
        public const string ISSUER = "AuthDemoAirlinesServer";
        public const string AUDIENCE = "AuthDemoAirlinesClient";
        const string KEY = "supersecretkeytoencodetoken!2303";
        public const int LIFETIME = 1; // 1 минута
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
