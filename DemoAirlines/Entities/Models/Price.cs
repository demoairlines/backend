﻿using DemoAirlines.Entities.Enums;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DemoAirlines.Entities.Models
{
    public class PriceList
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid FlightId { get; set; }
        public Flight Flight { get; set; }

        [Required]
        public FlightClass Class { get; set; }

        [Required]
        public FlightTariff Tariff { get; set; }

        [Required]
        public Decimal Price { get; set; }

        public string Description { get; set; }
        
    }
}
