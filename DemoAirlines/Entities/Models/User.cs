﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DemoAirlines.Entities.Models
{
    public class User
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string Role { get; set; }

        public Passenger Passenger { get; set; }
    }
}
