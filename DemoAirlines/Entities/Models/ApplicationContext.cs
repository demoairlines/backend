﻿using Microsoft.EntityFrameworkCore;
using DemoAirlines.Entities.Models;

namespace DemoAirlines.Entities.Models
{
    public class ApplicationContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Airport> Airports { get; set; }
        public DbSet<Passenger> Passenger { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }

        public DbSet<DemoAirlines.Entities.Models.PriceList> PriceList { get; set; }

        public DbSet<DemoAirlines.Entities.Models.Ticket> Ticket { get; set; }

        public DbSet<DemoAirlines.Entities.Models.Flight> Flight { get; set; }

    }
}
