﻿using DemoAirlines.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace DemoAirlines.Entities.Models
{
    public class Passenger
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string SecondName { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public string Phone { get; set; }
        public string EmergencyPhone { get; set; }

        [Required]
        public string Email { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Country { get; set; }

        [Required]
        public DocumentType DocumentType { get; set; }
        [Required]
        public string DocumentNumber { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
