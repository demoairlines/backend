﻿using DemoAirlines.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace DemoAirlines.Entities.Models
{
    public class Ticket
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid FlightId { get; set; }
        public Flight Flight { get; set; }

        [Required]
        public Guid PriceId { get; set; }
        public PriceList Price { get; set; }

        [Required]
        public Guid PassengerId { get; set; }
        public Passenger Passenger { get; set; }

        public PaymentStatus PaymentStatus { get; set; }
    }
}
