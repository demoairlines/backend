﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DemoAirlines.Entities.Models
{
    public class Flight
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid OriginAirportId { get; set; }
        public Airport OriginAirport { get; set; }

        [Required]
        public Guid DestinationAirportId { get; set; }
        public Airport DestAirport { get; set; }

        [Required]
        public DateTime DepartureTime { get; set; }
        [Required]
        public DateTime ArrivalTime { get; set; }

    }
}
