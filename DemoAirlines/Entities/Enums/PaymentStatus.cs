﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DemoAirlines.Entities.Enums
{
    public enum PaymentStatus
    {
        [Description("Заказ еще не оплачен")]
        Waiting = 0,

        [Description("Оплата прошла успешно")]
        Success = 1,

        [Description("Ошибка при оплате")]
        Fail = 2,

        [Description("Бронь")]
        Reserve = 3
    }
}
