﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoAirlines.Entities.Enums
{
    public enum FlightClass
    {
        Econom = 0,
        Business = 1
    }
}
