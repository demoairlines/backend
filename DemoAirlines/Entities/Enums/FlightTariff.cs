﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoAirlines.Entities.Enums
{
    public enum FlightTariff
    {
        Basic = 0,
        Classic = 1,
        Plus = 2,
        PartiallyFlexible = 3,
        Flexible = 4,
        FullyFlexible = 5,
    }
}
