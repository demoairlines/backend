﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoAirlines.Entities.Enums
{
    public enum DocumentType
    {
        Passport = 0,
        IdCard = 1
    }
}
